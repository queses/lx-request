var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { AxiosMiddlewareRegistry } from '../AxiosMiddlewareRegistry';
export const getBaseApiAxiosOptions = (apiHost, token, basePath = '') => {
    const options = {
        withCredentials: true,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest'
        },
        baseURL: apiHost + basePath,
        params: {}
    };
    if (token) {
        options.headers['Authorization'] = 'Bearer ' + token;
    }
    return options;
};
export const applyMiddlewaresOnAxiosInstance = (axiosInstance) => {
    axiosInstance.interceptors.request.use((config) => {
        return AxiosMiddlewareRegistry.inst.beforeRequest(config);
    }, (error) => __awaiter(this, void 0, void 0, function* () {
        yield AxiosMiddlewareRegistry.inst.errorBeforeRequest(error);
        throw error;
    }));
    axiosInstance.interceptors.response.use((res) => {
        return AxiosMiddlewareRegistry.inst.afterRequest(res);
    }, (error) => __awaiter(this, void 0, void 0, function* () {
        yield AxiosMiddlewareRegistry.inst.errorAfterRequest(error);
        throw error;
    }));
    return axiosInstance;
};
//# sourceMappingURL=lx-request-axios-utils.js.map