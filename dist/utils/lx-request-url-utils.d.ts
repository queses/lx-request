export declare const getLxApiPath: (path?: string) => string;
export declare const getApiRefreshTokenUrl: () => string;
export declare const getApiCheckTokenUrl: () => string;
