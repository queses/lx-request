import { AxiosInstance, AxiosRequestConfig } from 'axios';
export declare const getBaseApiAxiosOptions: (apiHost: string, token: string | undefined, basePath?: string) => AxiosRequestConfig;
export declare const applyMiddlewaresOnAxiosInstance: (axiosInstance: AxiosInstance) => AxiosInstance;
