export const parseJwtPayload = (token) => {
    if (!token) {
        return;
    }
    const fistDotIndex = token.indexOf('.');
    const secondDotIndex = token.indexOf('.', fistDotIndex + 1);
    const payload = (fistDotIndex >= 0)
        ? token.substring(fistDotIndex + 1, (secondDotIndex >= 0) ? secondDotIndex : undefined)
        : token;
    let decoded = (typeof window === 'undefined')
        ? Buffer.from(payload, 'base64').toString('binary')
        : atob(payload);
    try {
        return JSON.parse(decoded);
    }
    catch (e) {
        return;
    }
};
//# sourceMappingURL=lx-request-jwt-utils.js.map