var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import axios from 'axios';
import { applyMiddlewaresOnAxiosInstance, getBaseApiAxiosOptions } from './utils/lx-request-axios-utils';
import { JwtRefreshError, JwtRefreshTokenOutdatedError } from './errors';
import { getApiCheckTokenUrl, getApiRefreshTokenUrl } from './utils/lx-request-url-utils';
import { parseJwtPayload } from './utils/lx-request-jwt-utils';
export class AxiosInstanceSsr {
    constructor(apiHost, authApiHost, authToken, refreshToken, cookieString, baseUrl = '', extraHeaders, customUrls = {}) {
        this.apiHost = apiHost;
        this.authApiHost = authApiHost;
        this.authToken = authToken;
        this.refreshToken = refreshToken;
        this.cookieString = cookieString;
        this.baseUrl = baseUrl;
        this.extraHeaders = extraHeaders;
        this.customUrls = customUrls;
        this.checkApiUrl = customUrls.checkToken || getApiCheckTokenUrl();
        this.refreshApiUrl = customUrls.refreshToken || getApiRefreshTokenUrl();
        this.axios = this.createAxios(extraHeaders);
    }
    get instance() {
        return this.axios;
    }
    updateInstance(newRefreshToken, newAuthToken) {
        if (typeof newAuthToken !== 'undefined') {
            this.authToken = newAuthToken;
        }
        if (typeof newRefreshToken !== 'undefined') {
            this.refreshToken = newRefreshToken;
        }
        this.axios = this.createAxios();
        return this;
    }
    getTokenPayload() {
        return parseJwtPayload(this.authToken);
    }
    checkToken() {
        return __awaiter(this, void 0, void 0, function* () {
            let payload = parseJwtPayload(this.authToken);
            if (!payload || !payload.exp) {
                return 0;
            }
            const left = Math.floor(payload.exp - Date.now() / 1000);
            return (left > 0) ? left : 0;
        });
    }
    refreshAuthToken() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.refreshToken) {
                throw new JwtRefreshError('Trying to refresh auth without refresh token');
            }
            const reqOptions = this.getAuthRequestConf({
                headers: { 'Cookie': this.cookieString }
            });
            let res;
            try {
                res = yield this.axios.get(this.refreshApiUrl, reqOptions);
            }
            catch (err) {
                if (err.response && err.response.status === 401) {
                    throw new JwtRefreshTokenOutdatedError();
                }
                else if (err.response) {
                    throw new JwtRefreshError('', err.response.status, err.response.data);
                }
                else {
                    throw err;
                }
            }
            const { token } = res.data;
            const left = res.data.duration;
            return { left, token };
        });
    }
    createAxios(extraHeaders) {
        const options = getBaseApiAxiosOptions(this.apiHost, this.authToken, this.baseUrl);
        if (extraHeaders) {
            Object.assign(options.headers, extraHeaders);
        }
        return applyMiddlewaresOnAxiosInstance(axios.create(options));
    }
    getAuthRequestConf(extraConfig = {}) {
        return Object.assign({
            baseURL: this.authApiHost
        }, extraConfig);
    }
}
//# sourceMappingURL=AxiosInstanceSsr.js.map