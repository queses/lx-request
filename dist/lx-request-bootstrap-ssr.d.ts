import { TAuthUrls, TExtraRequestInstance, TSsrGetCookieFromReq, TSsrOnConfigUpdated, TSsrOnNoAuth, TSsrOnRefreshTokenInvalid, TSsrSaveToken, TSsrSaveTokenTtl } from './types';
export declare const lxRequestBootstrapSsr: (apiHost: string, authApiHost: string, getCookieFromRequestCb: TSsrGetCookieFromReq, saveAuthTokenTtlCb: TSsrSaveTokenTtl, saveAuthTokenCb: TSsrSaveToken, onConfigUpdatedCb: TSsrOnConfigUpdated, extraRequestInstances?: TExtraRequestInstance[], onNoAuth?: TSsrOnNoAuth | undefined, onRefreshTokenIsInvalidCb?: TSsrOnRefreshTokenInvalid | undefined, ssrExtraHeaders?: {
    [key: string]: string;
} | undefined, customUrls?: TAuthUrls | undefined) => Promise<void>;
