var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { AxiosInstancesRegistry } from './AxiosInstancesRegistry';
import { AxiosInstanceSsr } from './AxiosInstanceSsr';
import { JwtRefreshTokenOutdatedError } from './errors';
import { cookieParser } from './utils';
export const lxRequestBootstrapSsr = (apiHost, authApiHost, getCookieFromRequestCb, saveAuthTokenTtlCb, saveAuthTokenCb, onConfigUpdatedCb, extraRequestInstances = [], onNoAuth, onRefreshTokenIsInvalidCb, ssrExtraHeaders, customUrls) => __awaiter(this, void 0, void 0, function* () {
    onNoAuth = onNoAuth ? onNoAuth : () => undefined;
    onRefreshTokenIsInvalidCb = onRefreshTokenIsInvalidCb || onNoAuth;
    const cookie = getCookieFromRequestCb();
    const refreshToken = cookieParser('lx-jwt-refresh', cookie);
    const authToken = cookieParser('lx-jwt-auth', cookie);
    const axios = new AxiosInstanceSsr(apiHost, authApiHost, authToken, refreshToken, cookie, undefined, ssrExtraHeaders, customUrls);
    AxiosInstancesRegistry.add(axios);
    extraRequestInstances.forEach(({ baseUrl, key, host }) => {
        if (!key) {
            return;
        }
        AxiosInstancesRegistry.add(new AxiosInstanceSsr(host || apiHost, authApiHost, authToken, '', cookie, baseUrl, ssrExtraHeaders, customUrls), key);
    });
    if (refreshToken) {
        let left = yield checkTokenAndGetLeft(axios);
        if (left < 30) {
            const refreshResult = yield refreshTokenAndGetTokenWithLeft(axios, onRefreshTokenIsInvalidCb);
            if (!refreshResult) {
                yield onNoAuth();
                return;
            }
            AxiosInstancesRegistry.updateInstances(undefined, refreshResult.token);
            left = refreshResult.left;
            saveAuthTokenCb(refreshResult.token);
        }
        saveAuthTokenTtlCb(left);
    }
    else {
        yield onNoAuth();
    }
    onConfigUpdatedCb();
});
const checkTokenAndGetLeft = (axios) => axios.checkToken();
const refreshTokenAndGetTokenWithLeft = (axios, onRefreshTokenIsInvalidCb) => __awaiter(this, void 0, void 0, function* () {
    try {
        return yield axios.refreshAuthToken();
    }
    catch (err) {
        if (err instanceof JwtRefreshTokenOutdatedError) {
            yield onRefreshTokenIsInvalidCb(err);
        }
        else {
            throw err;
        }
    }
});
//# sourceMappingURL=lx-request-bootstrap-ssr.js.map