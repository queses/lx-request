var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { AxiosInstancesRegistry } from './AxiosInstancesRegistry';
import { AxiosInstanceClient } from './AxiosInstanceClient';
import { LxAuthClientLoginService } from './LxAuthClientLoginService';
export const lxRequestBootstrapClient = (apiHost, authApiHost, ssrTokenTtl, ssrToken, ssrFetchUserFailed, getCookieCb, setCookieCb, removeCookieCb, onFetchUserFailedCb, onConfigUpdatedCb, extraRequestInstances = [], customUrls) => {
    LxAuthClientLoginService.configure(setCookieCb, removeCookieCb, onConfigUpdatedCb);
    if (ssrFetchUserFailed) {
        onFetchUserFailedCb();
    }
    if (ssrToken) {
        LxAuthClientLoginService.inst.setAuthTokenCookie(ssrToken, ssrTokenTtl);
    }
    const getTokenCb = () => getCookieCb('lx-jwt-auth');
    const authToken = (ssrFetchUserFailed) ? '' : getTokenCb();
    AxiosInstancesRegistry.add(new AxiosInstanceClient(apiHost, authApiHost, authToken, undefined, customUrls));
    extraRequestInstances.forEach(({ baseUrl, key, host }) => {
        if (key) {
            AxiosInstancesRegistry.add(new AxiosInstanceClient(host || apiHost, authApiHost, authToken, baseUrl, customUrls), key);
        }
    });
    if (authToken) {
        initTokenRefreshing(ssrTokenTtl, getTokenCb, onConfigUpdatedCb);
    }
    onConfigUpdatedCb();
};
const initTokenRefreshing = (initialLeft, getTokenCb, onConfigUpdatedCb) => {
    let initialMsLeft = (initialLeft - 30) * 1000;
    if (initialMsLeft < 0) {
        initialMsLeft = 0;
    }
    const timeoutId = setTimeout(() => __awaiter(this, void 0, void 0, function* () {
        clearTimeout(timeoutId);
        const left = yield refreshToken(getTokenCb, onConfigUpdatedCb);
        const msLeft = (left - 30) * 1000;
        setInterval(() => refreshToken(getTokenCb, onConfigUpdatedCb), msLeft);
    }), initialMsLeft);
};
const refreshToken = (getTokenCb, onConfigUpdatedCb) => __awaiter(this, void 0, void 0, function* () {
    const axios = AxiosInstancesRegistry.get();
    const left = yield axios.refreshToken();
    AxiosInstancesRegistry.updateInstances(getTokenCb());
    onConfigUpdatedCb();
    return left;
});
//# sourceMappingURL=lx-request-bootstrap-client.js.map