var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// Singleton:
export class AxiosMiddlewareRegistry {
    constructor() {
        this.beforeHandlers = [];
        this.afterHandlers = [];
        this.errorBeforeHandlers = [];
        this.errorAfterHandlers = [];
    }
    beforeRequest(config) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const handler of this.beforeHandlers) {
                const handlerConfig = yield handler(Object.assign({}, config));
                if (handlerConfig) {
                    config = Object.assign(config, handlerConfig);
                }
            }
            return config;
        });
    }
    errorBeforeRequest(requestError) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const handler of this.errorBeforeHandlers) {
                yield handler(Object.assign({}, requestError));
            }
            return requestError;
        });
    }
    afterRequest(response) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const handler of this.afterHandlers) {
                const handlerResponse = yield handler(Object.assign({}, response));
                if (handlerResponse) {
                    response = Object.assign(response, handlerResponse);
                }
            }
            return response;
        });
    }
    errorAfterRequest(responseError) {
        return __awaiter(this, void 0, void 0, function* () {
            for (const handler of this.errorAfterHandlers) {
                yield handler(Object.assign({}, responseError));
            }
            return responseError;
        });
    }
    registerBeforeHandler(handler) {
        this._checkHandler(handler);
        this.beforeHandlers.push(handler);
    }
    registerAfterHandler(handler) {
        this._checkHandler(handler);
        this.afterHandlers.push(handler);
    }
    registerErrorBeforeHandler(handler) {
        this._checkHandler(handler);
        this.errorBeforeHandlers.push(handler);
    }
    registerErrorAfterHandler(handler) {
        this._checkHandler(handler);
        this.errorAfterHandlers.push(handler);
    }
    _checkHandler(handler) {
        if (typeof handler !== 'function') {
            throw new TypeError('Axios handler should be a function');
        }
    }
    static get inst() {
        if (!instance) {
            instance = new this();
        }
        return instance;
    }
}
let instance;
//# sourceMappingURL=AxiosMiddlewareRegistry.js.map