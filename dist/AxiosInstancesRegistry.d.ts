import { TAnyAxiosInstance } from './types';
export declare class AxiosInstancesRegistry {
    private instances;
    static add(instance: TAnyAxiosInstance, key?: string): void;
    static get<T extends TAnyAxiosInstance>(key?: string): T;
    static updateInstances(refreshToken?: string, authKey?: string): void;
    static readonly inst: AxiosInstancesRegistry;
}
