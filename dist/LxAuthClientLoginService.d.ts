import { TClientOnConfigUpdated, TClientRemoveCookie, TClientSetCookie } from './types';
export declare class LxAuthClientLoginService {
    private setCookieCb;
    private removeCookieCb;
    private onConfigUpdatedCb?;
    static configure(setCookieCb: TClientSetCookie, removeCookieCb: TClientRemoveCookie, onConfigUpdatedCb?: TClientOnConfigUpdated): void;
    setAuthTokenCookie(authToken: string, durationSec?: number): void;
    refreshTokenOnAuthorityChange(): Promise<void>;
    refreshTokenOnRouteChange(): Promise<any> | void;
    onLogin(authToken: string, duration: number): void;
    onLogout(): void;
    static readonly inst: LxAuthClientLoginService;
}
