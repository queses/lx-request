import { AxiosInstance } from 'axios';
import { TAuthUrls } from './types';
export declare class AxiosInstanceClient {
    private apiHost;
    private authApiHost;
    private authToken;
    private baseUrl;
    private customUrls;
    private readonly refreshApiUrl;
    private axios;
    constructor(apiHost: string, authApiHost: string, authToken: string, baseUrl?: string, customUrls?: TAuthUrls);
    readonly instance: AxiosInstance;
    readonly authJwt: string;
    updateInstance(newAuthToken?: string): this;
    refreshToken(): Promise<any>;
    _createAxios(): AxiosInstance;
    _getAuthRequestConf(extraConfig?: {}): {
        baseURL: string;
    };
}
