import { AxiosInstance } from 'axios';
import { TAuthUrls } from './types';
export declare class AxiosInstanceSsr {
    private apiHost;
    private authApiHost;
    private authToken;
    private refreshToken;
    private cookieString;
    private baseUrl;
    private extraHeaders?;
    private customUrls;
    private readonly checkApiUrl;
    private readonly refreshApiUrl;
    private axios;
    constructor(apiHost: string, authApiHost: string, authToken: string | undefined, refreshToken: string | undefined, cookieString: string, baseUrl?: string, extraHeaders?: {
        [key: string]: string;
    } | undefined, customUrls?: TAuthUrls);
    readonly instance: AxiosInstance;
    updateInstance(newRefreshToken?: string, newAuthToken?: string): this;
    getTokenPayload(): {
        [key: string]: any;
    } | undefined;
    checkToken(): Promise<number>;
    refreshAuthToken(): Promise<{
        left: any;
        token: any;
    }>;
    private createAxios;
    private getAuthRequestConf;
}
