import { TAfterRequestErrorHandler, TAfterRequestHandler, TBeforeRequestErrorHandler, TBeforeRequestHandler } from './types';
import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
export declare class AxiosMiddlewareRegistry {
    private beforeHandlers;
    private afterHandlers;
    private errorBeforeHandlers;
    private errorAfterHandlers;
    beforeRequest(config: AxiosRequestConfig): Promise<AxiosRequestConfig>;
    errorBeforeRequest(requestError: AxiosError): Promise<AxiosError<any>>;
    afterRequest(response: AxiosResponse): Promise<AxiosResponse<any>>;
    errorAfterRequest(responseError: AxiosError): Promise<AxiosError<any>>;
    registerBeforeHandler(handler: TBeforeRequestHandler): void;
    registerAfterHandler(handler: TAfterRequestHandler): void;
    registerErrorBeforeHandler(handler: TBeforeRequestErrorHandler): void;
    registerErrorAfterHandler(handler: TAfterRequestErrorHandler): void;
    private _checkHandler;
    static readonly inst: AxiosMiddlewareRegistry;
}
