// Singleton:
export class AxiosInstancesRegistry {
    constructor() {
        this.instances = {};
    }
    static add(instance, key = 'default') {
        this.inst.instances[key] = instance;
    }
    static get(key = 'default') {
        return this.inst.instances[key];
    }
    static updateInstances(refreshToken, authKey) {
        Object.keys(this.inst.instances).map((instanceKey) => {
            this.inst.instances[instanceKey].updateInstance(refreshToken, authKey);
        });
    }
    static get inst() {
        if (!instance) {
            instance = new this();
        }
        return instance;
    }
}
let instance;
//# sourceMappingURL=AxiosInstancesRegistry.js.map