import { cookieParser } from './utils';
import { AxiosInstancesRegistry } from './AxiosInstancesRegistry';
// Singleton:
export class LxAuthClientLoginService {
    static configure(setCookieCb, removeCookieCb, onConfigUpdatedCb) {
        this.inst.setCookieCb = setCookieCb;
        this.inst.removeCookieCb = removeCookieCb;
        this.inst.onConfigUpdatedCb = onConfigUpdatedCb;
    }
    setAuthTokenCookie(authToken, durationSec) {
        let expiresAt;
        if (durationSec) {
            expiresAt = new Date();
            expiresAt.setTime(expiresAt.getTime() + durationSec * 1000);
        }
        this.setCookieCb('lx-jwt-auth', authToken, expiresAt);
    }
    refreshTokenOnAuthorityChange() {
        const inst = AxiosInstancesRegistry.get();
        return inst.refreshToken().then((duration) => {
            AxiosInstancesRegistry.updateInstances(inst.authJwt);
            this.setAuthTokenCookie(inst.authJwt, duration);
            if (typeof this.onConfigUpdatedCb === 'function') {
                this.onConfigUpdatedCb();
            }
        }).catch(err => { throw err; });
    }
    refreshTokenOnRouteChange() {
        if (typeof document !== 'undefined' && !cookieParser('lx-jwt-auth', document.cookie)) {
            const inst = AxiosInstancesRegistry.get();
            if (!inst.authJwt) {
                return;
            }
            return inst.refreshToken().catch(err => { throw err; }).then(() => {
                AxiosInstancesRegistry.updateInstances(inst.authJwt);
                if (typeof this.onConfigUpdatedCb === 'function') {
                    this.onConfigUpdatedCb();
                }
            });
        }
    }
    onLogin(authToken, duration) {
        this.setAuthTokenCookie(authToken, duration);
    }
    onLogout() {
        this.removeCookieCb('lx-jwt-auth');
    }
    static get inst() {
        if (!instance) {
            instance = new this();
        }
        return instance;
    }
}
let instance;
//# sourceMappingURL=LxAuthClientLoginService.js.map