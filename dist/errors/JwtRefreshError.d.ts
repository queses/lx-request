import { JwtTokenCheckError } from './JwtTokenCheckError';
export declare class JwtRefreshError extends JwtTokenCheckError {
    name: string;
    constructor(message?: string, responseStatus?: number, responseData?: any);
}
