import { JwtTokenCheckError } from './JwtTokenCheckError';
export class JwtRefreshError extends JwtTokenCheckError {
    constructor(message = '', responseStatus, responseData) {
        super(message || 'Error while refreshing JWT token', responseStatus, responseData);
        this.name = 'JwtRefreshError';
    }
}
//# sourceMappingURL=JwtRefreshError.js.map