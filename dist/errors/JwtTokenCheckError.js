export class JwtTokenCheckError extends Error {
    constructor(message = '', responseStatus, responseData) {
        super();
        this.name = 'JwtTokenCheckError';
        this.message = message || 'Error while checking JWT token';
        if (responseStatus) {
            this.message += '. Response status: ' + responseStatus;
        }
        this.response = {
            status: responseStatus,
            data: responseData
        };
    }
}
//# sourceMappingURL=JwtTokenCheckError.js.map