export class JwtRefreshTokenOutdatedError extends Error {
    constructor(message = '') {
        super(message || 'JWT refresh token is outdated');
        this.name = 'JwtRefreshTokenOutdatedError';
    }
}
//# sourceMappingURL=JwtRefreshTokenOutdatedError.js.map