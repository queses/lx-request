export declare class JwtTokenCheckError extends Error {
    name: string;
    response: {
        status?: number;
        data?: any;
    };
    constructor(message?: string, responseStatus?: number, responseData?: any);
}
