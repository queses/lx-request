export declare class JwtTokenOutdatedError extends Error {
    name: string;
    constructor(message?: string);
}
