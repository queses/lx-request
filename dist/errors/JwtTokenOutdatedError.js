export class JwtTokenOutdatedError extends Error {
    constructor(message = '') {
        super();
        this.name = 'JwtTokenOutdatedError';
        this.message = message || 'JWT token is outdated';
    }
}
//# sourceMappingURL=JwtTokenOutdatedError.js.map