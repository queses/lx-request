export declare class JwtRefreshTokenOutdatedError extends Error {
    name: string;
    constructor(message?: string);
}
