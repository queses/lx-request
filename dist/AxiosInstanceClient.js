var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import axios from 'axios';
import { applyMiddlewaresOnAxiosInstance, getBaseApiAxiosOptions } from './utils/lx-request-axios-utils';
import { LxAuthClientLoginService } from './LxAuthClientLoginService';
import { getApiRefreshTokenUrl } from './utils/lx-request-url-utils';
export class AxiosInstanceClient {
    constructor(apiHost, authApiHost, authToken, baseUrl = '', customUrls = {}) {
        this.apiHost = apiHost;
        this.authApiHost = authApiHost;
        this.authToken = authToken;
        this.baseUrl = baseUrl;
        this.customUrls = customUrls;
        this.refreshApiUrl = customUrls.refreshToken || getApiRefreshTokenUrl();
        this.axios = this._createAxios();
    }
    get instance() {
        return this.axios;
    }
    get authJwt() {
        return this.authToken;
    }
    updateInstance(newAuthToken) {
        if (typeof newAuthToken !== 'undefined') {
            this.authToken = newAuthToken;
        }
        this.axios = this._createAxios();
        return this;
    }
    refreshToken() {
        return __awaiter(this, void 0, void 0, function* () {
            const res = yield this.axios.get(this.refreshApiUrl, this._getAuthRequestConf());
            LxAuthClientLoginService.inst.setAuthTokenCookie(res.data.token, res.data.duration);
            this.authToken = res.data.token;
            return res.data.duration;
        });
    }
    _createAxios() {
        return applyMiddlewaresOnAxiosInstance(axios.create(getBaseApiAxiosOptions(this.apiHost, this.authToken, this.baseUrl)));
    }
    _getAuthRequestConf(extraConfig = {}) {
        return Object.assign({
            baseURL: this.authApiHost
        }, extraConfig);
    }
}
//# sourceMappingURL=AxiosInstanceClient.js.map