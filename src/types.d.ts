import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { AxiosInstanceClient } from './AxiosInstanceClient'
import { AxiosInstanceSsr } from './AxiosInstanceSsr'

export interface TExtraRequestInstance {
  key: string
  baseUrl: string
  host?: string
}

export type TClientGetCookie = (cookieName: string) => any
export type TClientSetCookie = (cookieName: string, value: any, expires?: Date) => void
export type TClientRemoveCookie = (cookieName: string) => void
export type TClientOnUserFetchFailed = () => void
export type TClientOnConfigUpdated = () => void

export type TSsrGetCookieFromReq = () => string
export type TSsrSaveTokenTtl = (ttl: number) => void
export type TSsrSaveToken = (token: string) => void
export type TSsrOnNoAuth = () => void
export type TSsrOnRefreshTokenInvalid = (err: Error) => void
export type TSsrOnConfigUpdated = () => void

export type TBeforeRequestHandler = (config: AxiosRequestConfig) => AxiosRequestConfig | void | Promise<AxiosRequestConfig | void>
export type TBeforeRequestErrorHandler = (error: AxiosError) => void | Promise<void>
export type TAfterRequestHandler = (res: AxiosResponse) => AxiosResponse | void | Promise<AxiosResponse | void>
export type TAfterRequestErrorHandler = TBeforeRequestErrorHandler
export type TAnyRequestHandler = TBeforeRequestErrorHandler | TBeforeRequestHandler | TAfterRequestErrorHandler | TAfterRequestHandler
export type TAnyAxiosInstance = AxiosInstanceClient | AxiosInstanceSsr

export type TAuthUrls = {
  refreshToken?: string,
  checkToken?: string
}
