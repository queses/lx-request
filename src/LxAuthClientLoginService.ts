// Singleton:
import { TClientOnConfigUpdated, TClientRemoveCookie, TClientSetCookie } from './types'
import { cookieParser} from './utils'
import { AxiosInstancesRegistry } from './AxiosInstancesRegistry'
import { AxiosInstanceClient } from './AxiosInstanceClient'

// Singleton:
export class LxAuthClientLoginService {
  private setCookieCb: TClientSetCookie | any
  private removeCookieCb: TClientRemoveCookie | any
  private onConfigUpdatedCb?: TClientOnConfigUpdated

  static configure (setCookieCb: TClientSetCookie, removeCookieCb: TClientRemoveCookie, onConfigUpdatedCb?: TClientOnConfigUpdated) {
    this.inst.setCookieCb = setCookieCb
    this.inst.removeCookieCb = removeCookieCb
    this.inst.onConfigUpdatedCb = onConfigUpdatedCb
  }

  setAuthTokenCookie (authToken: string, durationSec?: number) {
    let expiresAt
    if (durationSec) {
      expiresAt = new Date()
      expiresAt.setTime(expiresAt.getTime() + durationSec * 1000)
    }

    this.setCookieCb('lx-jwt-auth', authToken, expiresAt)
  }

  refreshTokenOnAuthorityChange () {
    const inst = AxiosInstancesRegistry.get<AxiosInstanceClient>()
    return inst.refreshToken().then((duration: number) => {
      AxiosInstancesRegistry.updateInstances(inst.authJwt)
      this.setAuthTokenCookie(inst.authJwt, duration)

      if (typeof this.onConfigUpdatedCb === 'function') {
        this.onConfigUpdatedCb()
      }
    }).catch(err => { throw err })
  }

  refreshTokenOnRouteChange (): Promise<any> | void {
    if (typeof document !== 'undefined' && !cookieParser('lx-jwt-auth', document.cookie)) {
      const inst = AxiosInstancesRegistry.get<AxiosInstanceClient>()

      if (!inst.authJwt) {
        return
      }

      return inst.refreshToken().catch(err => { throw err }).then(() => {
        AxiosInstancesRegistry.updateInstances(inst.authJwt)

        if (typeof this.onConfigUpdatedCb === 'function') {
          this.onConfigUpdatedCb()
        }
      })
    }
  }

  onLogin (authToken: string, duration: number) {
    this.setAuthTokenCookie(authToken, duration)
  }

  onLogout () {
    this.removeCookieCb('lx-jwt-auth')
  }

  static get inst () {
    if (!instance) {
      instance = new this()
    }

    return instance
  }
}

let instance: LxAuthClientLoginService
