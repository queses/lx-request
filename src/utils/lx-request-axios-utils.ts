import { AxiosMiddlewareRegistry } from '../AxiosMiddlewareRegistry';
import { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'

export const getBaseApiAxiosOptions = (apiHost: string, token: string | undefined, basePath: string = '') => {
  const options: AxiosRequestConfig = {
    withCredentials: true,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    },
    baseURL: apiHost + basePath,
    params: {}
  }

  if (token) {
    options.headers['Authorization'] = 'Bearer ' + token
  }

  return options
}

export const applyMiddlewaresOnAxiosInstance = (axiosInstance: AxiosInstance) => {
  axiosInstance.interceptors.request.use(
    (config: AxiosRequestConfig) => {
      return AxiosMiddlewareRegistry.inst.beforeRequest(config)
    },
    async (error: AxiosError) => {
      await AxiosMiddlewareRegistry.inst.errorBeforeRequest(error)
      throw error
  })

  axiosInstance.interceptors.response.use(
    (res: AxiosResponse) => {
      return AxiosMiddlewareRegistry.inst.afterRequest(res)
    },
    async (error: AxiosError) => {
      await AxiosMiddlewareRegistry.inst.errorAfterRequest(error)
      throw error
  })

  return axiosInstance
}
