export const getLxApiPath = (path = '') => '/api/v1' + path

export const getApiRefreshTokenUrl = () => getLxApiPath('/user/refreshCookieRefresh')

export const getApiCheckTokenUrl = () => getLxApiPath('/user/refreshCookieCheckToken')
