import { AxiosInstancesRegistry } from './AxiosInstancesRegistry';
import { AxiosInstanceSsr } from './AxiosInstanceSsr';
import { JwtRefreshTokenOutdatedError } from './errors';
import {
  TAuthUrls,
  TExtraRequestInstance,
  TSsrGetCookieFromReq, TSsrOnConfigUpdated,
  TSsrOnNoAuth,
  TSsrOnRefreshTokenInvalid,
  TSsrSaveToken,
  TSsrSaveTokenTtl
} from './types'
import { cookieParser } from './utils'

export const lxRequestBootstrapSsr = async (
  apiHost: string,
  authApiHost: string,
  getCookieFromRequestCb: TSsrGetCookieFromReq,
  saveAuthTokenTtlCb: TSsrSaveTokenTtl,
  saveAuthTokenCb: TSsrSaveToken,
  onConfigUpdatedCb: TSsrOnConfigUpdated,
  extraRequestInstances: TExtraRequestInstance[] = [],
  onNoAuth?: TSsrOnNoAuth,
  onRefreshTokenIsInvalidCb?: TSsrOnRefreshTokenInvalid,
  ssrExtraHeaders?: { [key: string]: string },
  customUrls?: TAuthUrls
) => {
  onNoAuth = onNoAuth ? onNoAuth : () => undefined
  onRefreshTokenIsInvalidCb = onRefreshTokenIsInvalidCb || onNoAuth

  const cookie = getCookieFromRequestCb()
  const refreshToken = cookieParser('lx-jwt-refresh', cookie)
  const authToken = cookieParser('lx-jwt-auth', cookie)

  const axios = new AxiosInstanceSsr(
    apiHost, authApiHost, authToken, refreshToken, cookie, undefined, ssrExtraHeaders, customUrls
  )

  AxiosInstancesRegistry.add(axios)

  extraRequestInstances.forEach(({ baseUrl, key, host }) => {
    if (!key) {
      return
    }

    AxiosInstancesRegistry.add(new AxiosInstanceSsr(
      host || apiHost, authApiHost, authToken, '', cookie, baseUrl, ssrExtraHeaders, customUrls
    ), key)
  })

  if (refreshToken) {
    let left = await checkTokenAndGetLeft(axios)

    if (left < 30) {
      const refreshResult = await refreshTokenAndGetTokenWithLeft(axios, onRefreshTokenIsInvalidCb)
      if (!refreshResult) {
        await onNoAuth()
        return
      }

      AxiosInstancesRegistry.updateInstances(undefined, refreshResult.token)

      left = refreshResult.left
      saveAuthTokenCb(refreshResult.token)
    }

    saveAuthTokenTtlCb(left)
  } else {
    await onNoAuth()
  }

  onConfigUpdatedCb()
}

const checkTokenAndGetLeft = (axios: AxiosInstanceSsr) => axios.checkToken()

const refreshTokenAndGetTokenWithLeft = async (
  axios: AxiosInstanceSsr, onRefreshTokenIsInvalidCb: TSsrOnRefreshTokenInvalid
): Promise<{ left: number, token: string } | undefined> => {
  try {
    return await axios.refreshAuthToken()
  } catch (err) {
    if (err instanceof JwtRefreshTokenOutdatedError) {
      await onRefreshTokenIsInvalidCb(err)
    } else {
      throw err
    }
  }
}
