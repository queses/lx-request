import axios, { AxiosInstance } from 'axios'
import { applyMiddlewaresOnAxiosInstance, getBaseApiAxiosOptions } from './utils/lx-request-axios-utils'
import { LxAuthClientLoginService } from './LxAuthClientLoginService'
import { getApiRefreshTokenUrl } from './utils/lx-request-url-utils'
import { TAuthUrls } from './types'

export class AxiosInstanceClient {
  private readonly refreshApiUrl: string
  private axios: AxiosInstance

  constructor (
    private apiHost: string,
    private authApiHost: string,
    private authToken: string,
    private baseUrl = '',
    private customUrls: TAuthUrls = {}
  ) {
    this.refreshApiUrl = customUrls.refreshToken || getApiRefreshTokenUrl()
    this.axios = this._createAxios()
  }

  get instance () {
    return this.axios
  }

  get authJwt () {
    return this.authToken
  }

  updateInstance (newAuthToken?: string) {
    if (typeof newAuthToken !== 'undefined') {
      this.authToken = newAuthToken
    }

    this.axios = this._createAxios()
    return this
  }

  async refreshToken () {
    const res = await this.axios.get(this.refreshApiUrl, this._getAuthRequestConf())

    LxAuthClientLoginService.inst.setAuthTokenCookie(res.data.token, res.data.duration)
    this.authToken = res.data.token

    return res.data.duration
  }

  _createAxios () {
    return applyMiddlewaresOnAxiosInstance(
      axios.create(getBaseApiAxiosOptions(this.apiHost, this.authToken, this.baseUrl))
    )
  }

  _getAuthRequestConf (extraConfig = {}) {
    return Object.assign({
      baseURL: this.authApiHost
    }, extraConfig)
  }
}
