import {
  TAfterRequestErrorHandler,
  TAfterRequestHandler, TAnyRequestHandler,
  TBeforeRequestErrorHandler,
  TBeforeRequestHandler
} from './types'
import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'

// Singleton:
export class AxiosMiddlewareRegistry {
  private beforeHandlers: TBeforeRequestHandler[] = []
  private afterHandlers: TAfterRequestHandler[] = []
  private errorBeforeHandlers: TBeforeRequestErrorHandler[] = []
  private errorAfterHandlers: TAfterRequestErrorHandler[] = []

  async beforeRequest (config: AxiosRequestConfig) {
    for (const handler of this.beforeHandlers) {
      const handlerConfig = await handler(Object.assign({}, config))
      if (handlerConfig) {
        config = Object.assign(config, handlerConfig)
      }
    }
    
    return config
  }

  async errorBeforeRequest (requestError: AxiosError) {
    for (const handler of this.errorBeforeHandlers) {
      await handler(Object.assign({}, requestError))
    }

    return requestError
  }

  async afterRequest (response: AxiosResponse) {
    for (const handler of this.afterHandlers) {
      const handlerResponse = await handler(Object.assign({}, response))
      if (handlerResponse) {
        response = Object.assign(response, handlerResponse)
      }
    }

    return response
  }

  async errorAfterRequest (responseError: AxiosError) {
    for (const handler of this.errorAfterHandlers) {
      await handler(Object.assign({}, responseError))
    }

    return responseError
  }

  registerBeforeHandler (handler: TBeforeRequestHandler) {
    this._checkHandler(handler)
    this.beforeHandlers.push(handler)
  }

  registerAfterHandler (handler: TAfterRequestHandler) {
    this._checkHandler(handler)
    this.afterHandlers.push(handler)
  }

  registerErrorBeforeHandler (handler: TBeforeRequestErrorHandler) {
    this._checkHandler(handler)
    this.errorBeforeHandlers.push(handler)
  }

  registerErrorAfterHandler (handler: TAfterRequestErrorHandler) {
    this._checkHandler(handler)
    this.errorAfterHandlers.push(handler)
  }

  private _checkHandler (handler: TAnyRequestHandler) {
    if (typeof handler !== 'function') {
      throw new TypeError('Axios handler should be a function')
    }
  }

  static get inst () {
    if (!instance) {
      instance = new this()
    }

    return instance
  }
}

let instance: AxiosMiddlewareRegistry
