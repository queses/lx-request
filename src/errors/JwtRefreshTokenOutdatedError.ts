export class JwtRefreshTokenOutdatedError extends Error {
  name = 'JwtRefreshTokenOutdatedError'

  constructor (message: string = '') {
    super(message || 'JWT refresh token is outdated');
  }
}
