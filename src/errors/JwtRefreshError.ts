import { JwtTokenCheckError } from './JwtTokenCheckError';

export class JwtRefreshError extends JwtTokenCheckError {
  name = 'JwtRefreshError'

  constructor (message: string = '', responseStatus?: number, responseData?: any) {
    super(message || 'Error while refreshing JWT token', responseStatus, responseData);
  }
}
