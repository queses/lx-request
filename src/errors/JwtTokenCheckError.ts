export class JwtTokenCheckError extends Error {
  name = 'JwtTokenCheckError'
  response: { status?: number, data?: any }

  constructor (message: string = '', responseStatus?: number, responseData?: any) {
    super();
    this.message = message || 'Error while checking JWT token'
    if (responseStatus) {
      this.message += '. Response status: ' + responseStatus
    }

    this.response = {
      status: responseStatus,
      data: responseData
    }
  }
}
