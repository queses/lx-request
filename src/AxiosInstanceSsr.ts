import axios, { AxiosInstance } from 'axios'
import { applyMiddlewaresOnAxiosInstance, getBaseApiAxiosOptions } from './utils/lx-request-axios-utils';
import { JwtRefreshError, JwtRefreshTokenOutdatedError } from './errors';
import { getApiCheckTokenUrl, getApiRefreshTokenUrl } from './utils/lx-request-url-utils';
import { TAuthUrls } from './types'
import { parseJwtPayload } from './utils/lx-request-jwt-utils'

export class AxiosInstanceSsr {
  private readonly checkApiUrl: string
  private readonly refreshApiUrl: string
  private axios: AxiosInstance

  constructor (
    private apiHost: string,
    private authApiHost: string,
    private authToken: string | undefined,
    private refreshToken: string | undefined,
    private cookieString: string,
    private baseUrl = '',
    private extraHeaders?: { [key: string]: string },
    private customUrls: TAuthUrls = {}
  ) {
    this.checkApiUrl = customUrls.checkToken || getApiCheckTokenUrl()
    this.refreshApiUrl = customUrls.refreshToken || getApiRefreshTokenUrl()
    this.axios = this.createAxios(extraHeaders)
  }

  get instance () {
    return this.axios
  }

  updateInstance (newRefreshToken?: string, newAuthToken?: string) {
    if (typeof newAuthToken !== 'undefined') {
      this.authToken = newAuthToken
    }

    if (typeof newRefreshToken !== 'undefined') {
      this.refreshToken = newRefreshToken
    }

    this.axios = this.createAxios()
    return this
  }

  getTokenPayload () {
    return parseJwtPayload(this.authToken)
  }

  async checkToken () {
    let payload = parseJwtPayload(this.authToken)
    if (!payload || !payload.exp) {
      return 0
    }

    const left = Math.floor(payload.exp - Date.now() / 1000)
    return (left > 0) ? left : 0
  }

  async refreshAuthToken () {
    if (!this.refreshToken) {
      throw new JwtRefreshError('Trying to refresh auth without refresh token')
    }

    const reqOptions = this.getAuthRequestConf({
      headers: { 'Cookie': this.cookieString }
    })

    let res
    try {
      res = await this.axios.get(this.refreshApiUrl, reqOptions)
    } catch (err) {
      if (err.response && err.response.status === 401) {
        throw new JwtRefreshTokenOutdatedError()
      } else if (err.response) {
        throw new JwtRefreshError('', err.response.status, err.response.data)
      } else {
        throw err
      }
    }

    const { token } = res.data
    const left = res.data.duration

    return { left, token }
  }

  private createAxios (extraHeaders?: { [key: string]: string }) {
    const options = getBaseApiAxiosOptions(this.apiHost, this.authToken, this.baseUrl)

    if (extraHeaders) {
      Object.assign(options.headers, extraHeaders)
    }

    return applyMiddlewaresOnAxiosInstance(axios.create(options))
  }

  private getAuthRequestConf (extraConfig = {}) {
    return Object.assign({
      baseURL: this.authApiHost
    }, extraConfig)
  }
}

