import { TAnyAxiosInstance } from './types'

// Singleton:
export class AxiosInstancesRegistry {
  private instances: { [key: string]: TAnyAxiosInstance } = {}

  static add (instance: TAnyAxiosInstance, key = 'default') {
    this.inst.instances[key] = instance
  }

  static get <T extends TAnyAxiosInstance> (key = 'default'): T {
    return this.inst.instances[key] as T
  }

  static updateInstances (refreshToken?: string, authKey?: string) {
    Object.keys(this.inst.instances).map((instanceKey) => {
      this.inst.instances[instanceKey].updateInstance(refreshToken, authKey)
    })
  }

  static get inst () {
    if (!instance) {
      instance = new this()
    }

    return instance
  }
}

let instance: AxiosInstancesRegistry
