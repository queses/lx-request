import { AxiosInstancesRegistry } from './AxiosInstancesRegistry';
import { AxiosInstanceClient } from './AxiosInstanceClient';
import { LxAuthClientLoginService } from './LxAuthClientLoginService';
import {
  TAuthUrls,
  TClientGetCookie,
  TClientOnConfigUpdated, TClientOnUserFetchFailed,
  TClientRemoveCookie,
  TClientSetCookie,
  TExtraRequestInstance
} from './types'

export const lxRequestBootstrapClient = (
  apiHost: string,
  authApiHost: string,
  ssrTokenTtl: number,
  ssrToken: string,
  ssrFetchUserFailed: boolean,
  getCookieCb: TClientGetCookie,
  setCookieCb: TClientSetCookie,
  removeCookieCb: TClientRemoveCookie,
  onFetchUserFailedCb: TClientOnUserFetchFailed,
  onConfigUpdatedCb: TClientOnConfigUpdated,
  extraRequestInstances: TExtraRequestInstance[] = [],
  customUrls?: TAuthUrls
) => {
  LxAuthClientLoginService.configure(setCookieCb, removeCookieCb, onConfigUpdatedCb)

  if (ssrFetchUserFailed) {
    onFetchUserFailedCb()
  }

  if (ssrToken) {
    LxAuthClientLoginService.inst.setAuthTokenCookie(ssrToken, ssrTokenTtl)
  }

  const getTokenCb = () => getCookieCb('lx-jwt-auth')
  const authToken = (ssrFetchUserFailed) ? '' : getTokenCb()

  AxiosInstancesRegistry.add(new AxiosInstanceClient(apiHost, authApiHost, authToken, undefined, customUrls))
  extraRequestInstances.forEach(({ baseUrl, key, host }) => {
    if (key) {
      AxiosInstancesRegistry.add(
        new AxiosInstanceClient(host || apiHost, authApiHost, authToken, baseUrl, customUrls),
        key
      )
    }
  })

  if (authToken) {
    initTokenRefreshing(ssrTokenTtl, getTokenCb, onConfigUpdatedCb)
  }

  onConfigUpdatedCb()
}

const initTokenRefreshing = (initialLeft: number, getTokenCb: () => string, onConfigUpdatedCb: TClientOnConfigUpdated) => {
  let initialMsLeft = (initialLeft - 30) * 1000
  if (initialMsLeft < 0) {
    initialMsLeft = 0
  }

  const timeoutId = setTimeout(async () => {
    clearTimeout(timeoutId)

    const left = await refreshToken(getTokenCb, onConfigUpdatedCb)
    const msLeft = (left - 30) * 1000

    setInterval(() => refreshToken(getTokenCb, onConfigUpdatedCb), msLeft)
  }, initialMsLeft)
}

const refreshToken = async (getTokenCb: () => string, onConfigUpdatedCb: TClientOnConfigUpdated) => {
  const axios = AxiosInstancesRegistry.get() as AxiosInstanceClient
  const left = await axios.refreshToken()

  AxiosInstancesRegistry.updateInstances(getTokenCb())
  onConfigUpdatedCb()

  return left
}
